package s4w1exam;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
 
public class ReadFile {

    public static void readfile(File refFile) {

            FileReader refFileReader = null;
            int data;

            try {
                refFileReader = new FileReader(refFile);

                System.out.println();
                System.out.println("Reading File...");

                while((data=refFileReader.read()) != -1) {
                    System.out.print((char)data);
                    }

                }catch(IOException e){
                    e.printStackTrace();
                }

            finally {
                try {
                    refFileReader.close();
                }catch(IOException e) {
                    System.out.println("Exception handled while closing the file");
                }
            }


        }

    }