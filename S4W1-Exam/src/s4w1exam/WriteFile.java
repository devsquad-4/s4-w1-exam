package s4w1exam;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class WriteFile {

	public static void writeFile(File refFile) {

		// step 1
		Scanner sc = new Scanner(System.in);
		System.out.println("Write something to a file");
		String data = sc.nextLine();

		// Step 2
		FileWriter refFileWriter = null;

		try {
			refFileWriter = new FileWriter(refFile); // relative path

			for (int i = 0; i < data.length(); i++) {
				refFileWriter.write(data.charAt(i));
			}
		} catch (IOException e) {
			System.out.println("Exception handled while closing the file");
		}

		finally {
			try {
				refFileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception handled while closing the file");
			}
		}

	}
}