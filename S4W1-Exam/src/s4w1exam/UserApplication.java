package s4w1exam;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;


public class UserApplication {

	public static void main(String[] args) {
		Connection refConnection = null;
		PreparedStatement refPreparedStatement = null;
		User refUser = new User();
		
		//Get User Input
		Scanner scannerRef;

		scannerRef = new Scanner(System.in);

		System.out.println("Enter Customer ID : ");
		String userLoginID = scannerRef.nextLine();

		System.out.println("Enter Password : ");
		String userPassword = scannerRef.nextLine();

		refUser.setCustID(userLoginID);
		refUser.setCustPassword(userPassword);
		
		ResultSet rs = null;

		
		//Verify Credentials
		try {
			refConnection = DBUtility.getConnection();

			String sqlQuery = "SELECT customer_id,customer_password FROM customer WHERE customer_id=? and customer_password=?";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getCustID());
			refPreparedStatement.setString(2, refUser.getCustPassword());

			rs = refPreparedStatement.executeQuery();

			//User Authentication (Business Logic)
			while (rs.next()) {
				String customer_id = rs.getString("customer_id");
				String customer_password = rs.getString("customer_password");

				if (refUser.getCustID().equals(customer_id) && refUser.getCustPassword().equals(customer_password)) {
					System.out.println("User Authenticated");
					
					//Login success, create new file
					File refFile = new File("C:\\Users\\muhammad.ahmad\\eclipse-workspace\\S4W1-Exam\\src\\s4w1exam\\user.txt");
					
					// ask user to write to the file
			        WriteFile.writeFile(refFile);
			        
			        //Ask user if want to read file
			        System.out.println("1. Read File ");
			        System.out.println("2. Update File ");

					String choice = scannerRef.nextLine();
					
					switch (choice) {
					
					case "1":
						// read and display the file contents
				        ReadFile.readfile(refFile);
						break;
						
					case "2":
						//Update File
						System.out.println("Enter Text to append: ");
						
						String text = scannerRef.nextLine();
						
						FileWriter fr = new FileWriter(refFile, true);
						fr.write("\n"+text);
						fr.close();
						
						System.out.println("Read updated file? (Y/N): ");
						String choice2 = scannerRef.nextLine();

						if (choice2.equals("Y")) {
					        ReadFile.readfile(refFile);
						}
						
						break;
						
					default:
						break;
					}
					
					
			        
					
				} else if (!(refUser.getCustID().equals(customer_id) || !refUser.getCustPassword().equals(customer_password))){
					System.out.println("Invalid Credentials");
				}
				break;
			}
			
		} catch (Exception e) {
			System.out.println("Exception Handled while getting record.");

		}finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("\nClosing Connection..");
			}
		}
		
	}

}
